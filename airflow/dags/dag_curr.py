import sys
import os

from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime
from datetime import timedelta



SCRIPTS_PATH = '../../gcp'
DAG_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2021, 6, 22),
    'email_on_failure': False,
    'email_on_retry': False,
    'schedule_interval': '@daily',
    'retries': 1,
    'retry_delay': timedelta(seconds=5)
}


with DAG(
        dag_id='Curr_tasks',
        default_args=DAG_ARGS,
) as dag:
    
    t1 = BashOperator(
        task_id='load_data_to_gcs',
        bash_command='python3 {{params.LOAD_DATA_GCS}}',
        params = {'LOAD_DATA_GCS' : f'{SCRIPTS_PATH }/load_data_to_gcs.py'}
    )

    t2 = BashOperator(
        task_id='fill_dim_curr_table',
        bash_command='python3 {{params.FILL_DIM_CURR_TABLE}}',
        params = {'FILL_DIM_CURR_TABLE' : f'{SCRIPTS_PATH }/fill_dim_currency_table.py'}
    )

    t1 >> t2

