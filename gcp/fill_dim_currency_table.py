from config import config

from processing.features import *
from processing.data_management import (load_data_from_gcs, 
                                        write_table_to_bq
)

from google.cloud import bigquery


def main():


    client = bigquery.Client(credentials=config.bq_credentials)

    # load data from gcs
    token = f"{config.CREDENTIALS}/{config.GCS_CREDENTIALS}"
    currencies = load_data_from_gcs(
        token=token,
        project_id=config.PROJECT_ID,
        bucket_name=config.BUCKET_NAME,
        filename=config.DESTINATION_BLOB_NAME,
    )

    # calculate dim_currency_table
    results = update_dim_currency_table(df=currencies)

    # process results
    results = preprocess_dim_currency_table(df=results)

    # write results
    write_table_to_bq(
        client=client,
        df=results,
        dataset_id=config.DATASET_ID,
        table_id=config.TABLE_DIM_CURRENCY,
    )


if __name__ == "__main__":
    main()
