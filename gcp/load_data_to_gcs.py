import typing as t
from config import config

from google.cloud import storage

from processing.data_management import 	load_data_to_gcs

def main() -> None:

	gcs_client = storage.Client(credentials=config.gcs_credentials)

	load_data_to_gcs(
	    client=gcs_client,
	    bucket_name=config.BUCKET_NAME,
	    source_file_url=config.SOURCE_FILE_URL,
	    data_output=config.DATA_OUTPUT,
	    destination_blob_name=config.DESTINATION_BLOB_NAME,
	)

if __name__ == '__main__':
		main()