import re
import os
import wget
import typing as t
import multiprocessing as mp
from config import config
from processing.data_management import *

import gcsfs
import pandas as pd
import numpy as np

from google.cloud import storage
from google.cloud import bigquery
from google.oauth2 import service_account


def main() -> None:

    client = bigquery.Client(credentials=config.bq_credentials)

    # create a data set
    _ = bq_create_dataset(client=client, dataset_id=config.DATASET_ID)

    # create dim_currenccy table
    _ = bq_create_table(
        client=client,
        dataset_id=config.DATASET_ID,
        table_id=config.TABLE_DIM_CURRENCY,
        schema=config.DIM_CURRENCY_SCHEMA,
        date_col=config.DIM_CURRENCY_DATE,
    )

    # create fact_exchange_rate_history
    _ = bq_create_table(
        client=client,
        dataset_id=config.DATASET_ID,
        table_id=config.TABLE_FACT_EXCHANGE,
        schema=config.FACT_CURRENCY_RATE_HIST_SCHEMA,
        date_col=config.FACT_EXCHANGE_DATE,
    )


if __name__ == "__main__":
    main()
