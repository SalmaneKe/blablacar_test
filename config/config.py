from google.oauth2 import service_account
from google.cloud import bigquery


# GCP PROJECT
PROJECT_NAME = "blablacar-data-modeling"
PROJECT_ID = "blablacar-123"

# CLOUD STORAGE VARIABLES
BUCKET_NAME = "currencies_test"
GCS_SERVICE_ACCOUNT_ID = "blablacar-store-11"
DESTINATION_BLOB_NAME = "currencies"

DATA_OUTPUT = "data/curencies.csv"
CREDENTIALS = "./credentials"
GCS_CREDENTIALS = "gcs_credentials.json"
SOURCE_FILE_URL = 'http://webstat.banque-france.fr/fr/downloadFile.do?id=5385698&exportType=csv'

# CLOUD STORAGE CREDENTIALS
gcs_credentials = service_account.Credentials.from_service_account_file(
    filename=f"{CREDENTIALS}/{GCS_CREDENTIALS}",
    # scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

# ----------------------------------------------------------------------------------------------

# BIGQUERY VARIABLES
BQ_SERVICE_ACCOUNT_ID = "blablacar-bq-11"
BQ_CREDENTIALS = f"{CREDENTIALS}/bq_credentials.json"
DATASET_ID = "currencies"

GCS_FILENAME = "currencies"
TABLE_DIM_CURRENCY = "dim_currency"
TABLE_FACT_EXCHANGE = "fact_exchange_rate_history"
token = f"{CREDENTIALS}/gcs_list_credentials.json"

# DATES COLUMNS
DIM_CURRENCY_DATE = 'last_updated_date'
FACT_EXCHANGE_DATE = 'history_date'


# BIGQUERY CREDENTIALS
bq_credentials = service_account.Credentials.from_service_account_file(
    filename=f"{CREDENTIALS}/bq_credentials.json",
    # scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

# BIGQUERY TABLE SCHEMA
DIM_CURRENCY_SCHEMA = [
    bigquery.SchemaField("cur_code", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("one_euro_value", "FLOAT", mode="REQUIRED"),
    bigquery.SchemaField("last_updated_date", "DATE", mode="REQUIRED"),
    bigquery.SchemaField("Serial_code", "STRING", mode="REQUIRED"),
]

FACT_CURRENCY_RATE_HIST_SCHEMA = [
    bigquery.SchemaField("history_date", "DATE", mode="REQUIRED"),
    bigquery.SchemaField("from_cur_code", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("to_cur_code", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("exchange_rate", "FLOAT", mode="REQUIRED"),
]

## -------------------------------------------------------------------------------$$$



