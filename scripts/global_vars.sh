#export PROJECT_NAME= /to fill
#export PROJECT_ID= /to fill 
#export BILLING_ACCOUNT= /to fill

export OUTPUT=../data/curencies.csv
export GCS_SERVICE_ACCOUNT_ID=blablacar-store-11
export SOURCE_FILE_URL=http://webstat.banque-france.fr/fr/downloadFile.do?id=5385698&exportType=csv
export DESTINATION_BLOB_NAME=currencies
export GCS_CREDENTIALS=../credentials/gcs_credentials.json

export BUCKET_NAME=currencies_test
export STORAGE_CLASS=standard
export LOCATION=europe-west1

export BQ_SERVICE_ACCOUNT_ID=blablacar-bq-11
export BQ_CREDENTIALS=../credentials/bq_credentials.json