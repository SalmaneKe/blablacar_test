source global_vars.sh

gcloud init

# create a project
gcloud projects create $PROJECT_ID  --name=$PROJECT_NAME #--labels=$LABEL

# check project
gcloud projects list

# switch on this project
gcloud config set project blablacar-datamodeling

# link the billing account
#gcloud alpha billing accounts projects link my-project --billing-account=$BILLING_ACCOUNT

