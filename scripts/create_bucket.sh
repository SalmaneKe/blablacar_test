source global_vars.sh

# create the bucket
gsutil mb -p $PROJECT_ID -c $STORAGE_CLASS -l $LOCATION -b on gs://$BUCKET_NAME

# list buckets
gsutil ls