source global_vars.sh

gcloud init 

# Create a service account for GCS

gcloud iam service-accounts create $GCS_SERVICE_ACCOUNT_ID

gcloud projects add-iam-policy-binding $PROJECT_ID \
	--member=serviceAccount:$GCS_SERVICE_ACCOUNT_ID@$PROJECT_ID.iam.gserviceaccount.com \
    --role="roles/storage.admin"

gcloud iam service-accounts keys create ../credentials/gcs_credentials.json \
    --iam-account=$GCS_SERVICE_ACCOUNT_ID@$PROJECT_ID.iam.gserviceaccount.com



# Create service account for BigQuery

gcloud iam service-accounts create $BQ_SERVICE_ACCOUNT_ID

gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member=serviceAccount:$BQ_SERVICE_ACCOUNT_ID@$PROJECT_ID.iam.gserviceaccount.com \
    --role=roles/bigquery.admin

gcloud iam service-accounts keys create $BQ_CREDENTIALS \
    --iam-account=$BQ_SERVICE_ACCOUNT_ID@$PROJECT_ID.iam.gserviceaccount.com