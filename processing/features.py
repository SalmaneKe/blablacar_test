import re
import multiprocessing as mp

import pandas as pd
import numpy as np


def extract_cur_code(df: pd.DataFrame) -> np.array:

    """
    Extract the currency code from the second row
    Args:
        - df: the currency raw data

    return:
        returns an array of currencies
    """

    res = df.iloc[1, 1:].apply(lambda x: x[x.find("(") + 1 : x.find(")")]).values

    assert len(res)
    return res


def extract_one_euro_value(df: pd.DataFrame) -> np.array:

    """
    Extract the one-euro exchange value
    Args:
        - df: the currency raw data

    return:
        returns an array of one-euros equivalents
    """

    res = df.iloc[5, 1:].values

    assert len(res)
    return res


def extract_last_update_date(df: pd.DataFrame) -> np.array:

    """
    Extract the last update date
    Args:
        - df: the currency raw data

    return:
        returns an array of last update date
    """

    res = [df.iloc[5, 0]] * df.iloc[5, 1:].size
    assert len(res)
    return res


def extract_serial_code(df: pd.DataFrame) -> np.array:

    """
    Extract the serial code
    Args:
        - df: the currency raw data

    return:
        returns an array of lserial codes
    """

    res = df.iloc[0, 1:].values

    assert len(res)
    return res


def update_dim_currency_table(*, df: pd.DataFrame) -> pd.DataFrame:

    """
    Generate the dim-currency table
    Args:
        - df: the currency raw data

    return:
        dataframe of dim_currency table
    """

    if not isinstance(df, pd.DataFrame):
        raise TypeError("the input should be a pandas dataframe")
    if df.empty:
        raise ValueError("the dataframe should not be empty")

    results = pd.DataFrame(
        {
            "cur_code": extract_cur_code(df),
            "one_euro_value": extract_one_euro_value(df),
            "last_updated_date": extract_last_update_date(df),
            "Serial_code": extract_serial_code(df),
        }
    )

    return results


def preprocess_dim_currency_table(*, df: pd.DataFrame) -> pd.DataFrame:

    """
    Convert string values to floats
    Args:
        - df: the currency raw data

    return:
        processed dataframe
    """

    if not isinstance(df, pd.DataFrame):
        raise TypeError("the input should be a pandas dataframe")
    if df.empty:
        raise ValueError("the dataframe should not be empty")

    df["one_euro_value"] = (
        df.one_euro_value.str.replace(",", ".").astype(float).fillna(0)
    )

    return df


def get_currencies(df):

    """
    Get list of currencies
    Args:
        - df: the currency raw data

    return:
        retruns a list of currencies
    """

    df = df.copy()
    return [col[col.find("(") + 1 : col.find(")")] for col in df.iloc[1, 1:].values]


def create_cur_exch_rate_hist(df: pd.DataFrame, from_cur_code: str) -> pd.DataFrame:

    """
    Generate all past exchange rates from a given currency code
    Args:
        - df: the currency raw data
        - from_cur_code: a currency code to convert

    return:
        retruns a dataframe of past exchange rates
    """

    drop_rows = [
        "Code série :",
        "Unité :",
        "Magnitude :",
        "Méthode d'observation :",
        "Source :",
    ]

    # replace columns with currencies ids
    df = df.copy()
    df.columns = ["Date"] + get_currencies(df)

    # drop useless rows
    df = df.set_index("Date").drop(drop_rows).reset_index()

    # convert columns to floats
    for col in df.columns[1:]:
        df[col] = df[col].str.replace(",", ".").replace("-", np.nan).astype(float)

    # compute exchange rate
    cols = df.columns.drop(["Date", from_cur_code])
    rates = (df[cols].T / df[from_cur_code]).reset_index()
    rates.columns = ["to_cur_code"] + df["Date"].values.tolist()

    # reshape the results
    columns = ["history_date", "from_cur_code", "to_cur_code", "exchange_rate"]
    rates = (
        rates.melt(
            id_vars="to_cur_code",
            value_vars=rates.columns[1:].tolist(),
            var_name="history_date",
            value_name="exchange_rate",
        )
        .fillna(0)
        .assign(from_cur_code=from_cur_code)
        .reindex(columns=columns)
    )

    assert not rates.empty
    return rates


def update_fact_exchange_rate_history(*, df: pd.DataFrame) -> pd.DataFrame:

    """
    Generate all past exchange rates between all currencies
    Args:
        - df: the currency raw data
        - from_cur_code: a currency code to convert

    return:
        retruns a dataframe of past exchange rates between all currencies
    """

    # intit multiprocessing pool
    nprocs = mp.cpu_count()
    print(f"Number of CPU cores: {nprocs}")
    pool = mp.Pool(processes=nprocs)

    # calculate rates for each currency
    results = pool.starmap(
        create_cur_exch_rate_hist, [(df, cur) for cur in get_currencies(df)]
    )

    return pd.concat(results)
