import re
import os
import wget
import typing as t
from config import config
import multiprocessing as mp

import gcsfs
import pandas as pd
import numpy as np


from google.cloud import storage
from google.cloud import bigquery
from google.oauth2 import service_account


def load_data_to_gcs(
    *,
    client: service_account.Credentials,
    bucket_name: str,
    source_file_url: str,
    data_output: str,
    destination_blob_name: str,
) -> None:

    """
    Load a raw data to cloud storage from a URL

    Args:
        - client: gcs service account
        - bucket_name: the bucket in gcs where to store objects
        - source_file_url: url from which to retrieve the data
        - data_output: name of the file
        - destination_blob_name: the blob name

    """

    # Load data from url
    filename = wget.download(source_file_url, out=data_output)

    # upload the data in the bucket blob
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(filename)
    os.remove(filename)


def load_data_from_gcs(
    *, token: str, project_id: str, bucket_name: str, filename: str
) -> pd.DataFrame:

    """
    Load data from gcs in dataframe format

    Args:
        - token: key to access GCS file system
        - project_id: the current project_id
        - bucket_name: the current bucket name to retrieve the data
        - filename: the name of file to retrieve

    returns:
        returns the data in dataframe format

    """

    file_path = f"{bucket_name}/{filename}"

    fs = gcsfs.GCSFileSystem(token=token, project=project_id)
    with fs.open(file_path) as f:
        df = pd.read_csv(f, sep=";")
    return df


def bq_create_dataset(
    *, client: bigquery.client.Client, dataset_id: str
) -> bigquery.dataset.Dataset:

    """
    Create a Dataset in BigQuery

    Args:
        - client: bigquery client
        - dataset_id: the dataset identifier

    returns:
        returns the bigquery dataset object
    """

    dataset_name = client.project + "." + dataset_id
    dataset = bigquery.Dataset(dataset_name)
    dataset.location = "EU"
    dataset = client.create_dataset(dataset, timeout=30)  # Make an API request.
    print(f"Created dataset {client.project}.{dataset.dataset_id}")

    return dataset


def bq_create_table(
    *,
    client: bigquery.client.Client,
    dataset_id: str,
    table_id: str,
    schema: t.List[bigquery.schema.SchemaField],
    date_col: str,
) -> bigquery.table.Table:

    """
    Create a Dataset in BigQuery

    Args:
        - client: bigquery client
        - dataset_id: the dataset identifier
        - table_id: the table identifier
        - schema: the schema of the table
        - date_col: the date col corresponding which the table should be partitionned

    returns:
        returns the bigquery table object
    """

    table_name = client.project + "." + dataset_id + "." + table_id
    table = bigquery.Table(table_name, schema=schema)
    table.time_partitioning = bigquery.TimePartitioning(
        type_=bigquery.TimePartitioningType.DAY, field=date_col
    )
    table = client.create_table(table)
    print(f"Created table {table.project}.{table.dataset_id}.{ table.table_id}")

    return table


def write_table_to_bq(
    *, client: bigquery.client.Client, df: pd.DataFrame, dataset_id: str, table_id: str
) -> None:

    """
    Create a pandas dataframe to BigQuer

    Args:
        - client: bigquery client
        - df: pandas dataframe to write
        - dataset_id: the dataset identifier
        - table_id: the table identifier
    """
    dest_table = f"{dataset_id}.{table_id}"

    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.autodetect = True

    job = client.load_table_from_dataframe(
        dataframe=df, destination=dest_table, job_config=job_config
    )

    job.result()

    table = client.get_table(dest_table)  # Make an API request.
    print(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table
        )
    )
