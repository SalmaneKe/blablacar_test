# Technical test - Data Warehouse Team - BlaBlaCar



## Description

In this part of the test, we attemp to automate the ingestion of currencies dataset and extract insights in tables to be later on updated in database on daily basis.

GCP tools were used in order to build the data ETL pipeline.

## Getting Started

### Dependencies

Following libraries are used in this project
* pandas
* numpy
* wget
* typing
* gcsfs
* pandas
* google-cloud-storage
* google-cloud-bigquery


### Installing

* First pull the code in your local repository
* Create reate your local virtual environement in the project repository [link](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) 

* Install the requirements
```
pip3 intall -r requirements.txt
```

* Update the global variables in ```scripts/global_vars.sh``` with your own settings

* Install manually airflow by specifiying the variable AIRFLOW_HOME
```export AIRFLOW_HOME=/airflow```

### Executing program

* First create a project in your GCP Folder
```chmod +x ./scripts/*```
```cd scripts```
```./create_project.sh```


* Create the necessary service_accounts for this proejct
```./service_accounts.sh```


* Create a bucket in cloud storage
```./create_bucket.sh```

* Update the following variables in config.py according to your GCP settings
```PROJECT_NAME PROJECT_ID BUCKET_NAME```

* Initialise the Bigquery dataset and tables that would host the data
``` 
python3 ./gcp/init_bigquery_tables.py
```

* Load data from URL provided to GCS
```
python3 ./gcp/load_data_to_gcs.py
```

* Fill the table dim_currency_table.py in bigquery
```
python3 ./gcp/fill_dim_currency_table.py
```

* Fill the table dim_currency_table.py in bigquery (please note that the writing part in bigquery is commented as it is bit cumbersom)
```
python3 ./gcp/fill_fact_exch__rate_hist_table.py
```

* Airflow DAG is provided to automate the workflow. At this stage, it automates loading the raw data in cloud storage, calculating the dim_currency_table and store it in bigquery.



## Future improvements and takeaways 

* add unitary tests
* add code to calculate only the latest exchange rates in order to include it in the dag
* provide git_ci.yml to adope an approache CI/CD and store credentials in keys instead of credentials folder
* Ajuste airflow code in order to include GCP services and launch it in cloud composer




